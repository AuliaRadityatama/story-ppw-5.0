from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('record/', views.hal2, name='hal2'),
    path('kreasi/', views.hal3, name='hal3'),
    path('prototype/', views.hal0, name='hal0'),
]