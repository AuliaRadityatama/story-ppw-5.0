from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'story3.html')

def hal2(request):
    return render(request, 'story3_hal 2.html')

def hal3(request):
    return render(request, 'story3_hal kreasi.html')

def hal0(request):
    return render(request, 'story.html')